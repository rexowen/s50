import {useContext, useEffect} from 'react';
import { Redirect } from 'react-router-dom';
import UserContext from '../UserContext'; 

export default function Logout(){
		const { setUser, unsetUser } = useContext(UserContext)


		//Clear the localStorage of the user's information
		unsetUser();

		//Placing the 'setUser' setter function inside of a useEffect is necessary because
		//of updates within ReactJs that a state of another component cannot be updated while trying
		//to render a different component
		//By adding the useEffect, this will allow the logout page to render first  before
		//triggering the useEffect which changes the state of our user.
		useEffect(()=> {
			//Set the user state back to it's original value
			setUser({accessToken: null})
			// eslint-disable-next-line react-hooks/exhaustive-deps
		}, [])
		//the array in our useEffect provide initial rendering only. Once lang sya nag rerender

		return(
			//Redirected back to login
			< Redirect to="/login"/>
			)
}