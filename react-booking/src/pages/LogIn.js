import { Form, Button } from 'react-bootstrap';
import { Fragment, useState, useContext, useEffect} from 'react';
import Swal from 'sweetalert2';

//React Context
import UserContext from '../UserContext';

//routing
import { Redirect, useHistory } from 'react-router-dom';

export default function Login(){
		//The useHistroy hook gives you access to the history instance that you may use to nagivate or to access the location
		//
		const history = useHistory();
/*
useContext is a react hook used to unwrap our context. It will return the data passed as values
by a provider(UserContext.Provider component in App.js)
*/
	const {user, setUser} = useContext(UserContext);

	const [email, setEmail] = useState('');
	const [password1, setPassword1] = useState('');
	const [isActive, setIsActive] = useState(true);

	useEffect(()=> {
		if((email !== '' && password1 !== '')){
				setIsActive(true);
		}else{
				setIsActive(false);
		}
	}, [email, password1])

	function logInUser(e){
		e.preventDefault();

		//fetch has 2 arguments
		//1. URL from the server(routes)
		//2. optional object wwhich contains additional information about our requests such as method, headers, body etc.
		fetch('http://localhost:4000/users/login', {
			method: 'POST',
			headers: {
				'Content-Type': 'application/json'
			},
			body: JSON.stringify({
				email: email,
				password: password1
			})

		})
		.then(res => res.json())
		.then(data => {
			console.log(data)
			if(data.accessToken !== undefined){
				localStorage.setItem('accessToken', data.accessToken);
				setUser({accessToken: data.accessToken});

				Swal.fire({
					title: 'Yaaaay',
					icon: 'success',
					text: 'Thank you for logging in to Zuitt Booking System'
				})

				//Get user's details from our accesstoken
				fetch('http://localhost:4000/users/details', {
					headers: {
						Authorization: `Bearer ${data.accessToken}`
					}
				})
				.then(res => res.json())
				.then(data => {
					console.log(data)
					if(data.isAdmin === true){
						localStorage.setItem('email', data.email)
						setUser({
							email: data.email,
							isAdmin: data.isAdmin
						})

						//If admin, redirect the page to /courses
						//push(path - pushes a new entry onto the history stack)
						history.push('/courses');
					}else{
						history.push('/')
					}
				}) 
			}else{
				Swal.fire({
					title: 'Oooops',
					icon: 'error',
					text: 'Something Went Wrong. Check your Credentials.'
				})
			}
			setEmail('');
			setPassword1('')
		})
}
/*		setEmail('');
		setPassword1('');
		Swal.fire({
			title: 'Yaaay!!',
			icon: 'Success',
			text: 'You are now logged in!'
		})

		//
		localStorage.setItem('email',email);
		setUser({email: email})

		setEmail('')
		setPassword1('')


	//Create a conditional statement that will redirect the user to the 
	//homepage when a user is logged in.*/

	return(
		(user.accessToken !== null) ?
			<Redirect to="/"/>
			:

		<Fragment>
		<Form onSubmit={(e) => logInUser(e)}>
		<h1>LogIn</h1>
			<Form.Group>
				<Form.Label>Email Address: </Form.Label>
				<Form.Control
					type="email"
					placeholder="Enter email"
					value={email}
					onChange = {e => setEmail(e.target.value)}
					required
				 />
				 <Form.Text className="text-muted">
				 	We'll never share your email with anyone else.</Form.Text>
			</Form.Group>
			<Form.Group>
				<Form.Label>Password</Form.Label>
				<Form.Control
					type="password"
					placeholder="Enter your Password"
					value={password1}
					onChange = {e => setPassword1(e.target.value)}
					required
				/>
			</Form.Group>
			{isActive ?
			<Button variant ="primary" type="submit" id="submitBtn">LogIn</Button>
			:
			<Button variant ="primary" type="submit" id="submitBtn" disabled>LogIn</Button>
		}
		</Form>
		</Fragment>
		)
}