import { Form, Button } from 'react-bootstrap';
import { Fragment, useState, useContext, useEffect } from 'react';
import Swal from 'sweetalert2';
import { Redirect, useHistory } from 'react-router-dom';
import UserContext from '../UserContext'; 


export default function Register(){
	const history = useHistory();

	const {user} = useContext(UserContext);

	//State hooks to store the values of the input fields
	const [email, setEmail] = useState('');
	const [firstName, setFirstName] = useState('');
	const [lastName, setLastName] = useState('');
	const [mobileNo, setNumber] = useState('');
	const [password1, setPassword1] = useState('');
	const [password2, setPassword2] = useState('');
	const [isActive, setIsActive] = useState(true);

	useEffect(()=> {
		if((email !== '' && password1 !== '' && password2 !== '' && firstName !== '' && lastName !== '' & mobileNo !== '') && (password1 === password2)) {
				setIsActive(true);

		}else{
				setIsActive(false);
		}
	}, [firstName, lastName, mobileNo, email, password1, password2])

	function registerUser(e){
			e.preventDefault();

			fetch('http://localhost:4000/users/checkEmail', {
				method: 'POST',
				headers: {
					"Content-Type": "application/json"
				},
				body: JSON.stringify({
					email: email
				})
			})
			.then(res => res.json())
			.then(data => {
				console.log(data);
				if(data === true){
					Swal.fire({
						title: 'WOOPS!',
						icon: 'error',
						text: 'Email already exists!',
					})
				}else{
					fetch("http://localhost:4000/users/register", {
					method: 'POST',
					headers: {
						'Content-Type': 'application/json'
					},
					body: JSON.stringify({
						firstName: firstName,
						lastName: lastName,
						email: email,
						mobileNo: mobileNo,
						password: password2,
					})
				})
				.then(res => res.json())
				.then(data => {
					console.log(data)
					if(data === true){
						Swal.fire({
							title: "LET'S GO!",
							icon: "success",
							text: "WELCOME TO ZUITT!"
						})
						history.push("/login")
					}else{
						Swal.fire({
							title: "Try Again",
							icon: "error",
							text: "Please enter the required fields"
						})
					}
				})
					setFirstName("")
					setLastName("")
					setEmail("")
					setNumber("")
					setPassword2("")

				}
			})
				

		}

	//Two way binding
	/*
	The values in the fields of the form is bound to the getter of the state and the event
	is bount to the setter. This is called two way binding
	The date we changed in the view has updated the state
	The data in the state has updated the view

	*/

	/*
	Mini-Activity

	-Redirect the user back to "/" (homepage) route if a user is already logged in.



	*/

	return(
		(user.accessToken !== null)?
		<Redirect to="/login" />
		:
		<Fragment>
		<Form onSubmit={(e) => registerUser(e)}>
		<h1>Register</h1>
		<Form.Group>
				<Form.Label>First Name: </Form.Label>
				<Form.Control
					type="text"
					placeholder="Enter First Name"
					value={firstName}
					onChange = {e => setFirstName(e.target.value)}
					required
				 />
			</Form.Group>
			<Form.Group>
				<Form.Label>Last Name: </Form.Label>
				<Form.Control
					type="text"
					placeholder="Enter Last Name"
					value={lastName}
					onChange = {e => setLastName(e.target.value)}
					required
				 />
			</Form.Group>
			<Form.Group>
				<Form.Label>Email Address: </Form.Label>
				<Form.Control
					type="email"
					placeholder="Enter email"
					value={email}
					onChange = {e => setEmail(e.target.value)}
					required
				 />
				 <Form.Text className="text-muted">
				 	We'll never share your email with anyone else.</Form.Text>
			</Form.Group>
			<Form.Group>
				<Form.Label>Mobile Number: </Form.Label>
				<Form.Control
					type="tel"
					placeholder="Format 1234-567-8910"
					value={mobileNo}
					minlength = "11"
					onChange = {e => setNumber(e.target.value)}
					required
				 />
			</Form.Group>
			<Form.Group>
				<Form.Label>Password</Form.Label>
				<Form.Control
					type="password"
					placeholder="Enter your Password"
					value={password1}
					onChange = {e => setPassword1(e.target.value)}
					required
				/>
			</Form.Group>
			<Form.Group>
				<Form.Label>Verify Password</Form.Label>
				<Form.Control
					type="password"
					placeholder="Verify your Password"
					value={password2}
					onChange = {e => setPassword2(e.target.value)}
					required
				/>
			</Form.Group>
			{isActive ?
			<Button variant ="primary" type="submit" id="submitBtn">Submit</Button>
			:
			<Button variant ="primary" type="submit" id="submitBtn" disabled>Submit</Button>
		}
		</Form>
		</Fragment>
		)
}