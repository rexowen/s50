import { useState, useEffect, useContext } from 'react';

//bootstrap
import { Container } from 'react-bootstrap';
//components
import AdminView from '../Components/AdminView';
import UserView from '../Components/UserView';
//React Context
import UserContext from '../UserContext';


export default function Courses(){
	/*console.log(courseData);
	console.log(courseData[0]);*/

//for us to be able to display all the courses from the data file, we are going to use the map()
//The map() method loops through the individual course objeccts in our array and returns a component for each course
//Multiple comments createed through the map method must have a UNIQUE KEY that will help React js identify which /elements have been changed, added

// const courses = courseData.map(course => {
// 	return(
// 		<CourseCard key={course.id} courseProp={course}/>

// 		)
// })


// 	return(
// 		<Fragment>
// 		<h1>Courses</h1>
// 		{courses}
// 		</Fragment>
// 	)
// }

const { user } = useContext(UserContext);
const [allCourses, setAllCourses] = useState([]);
const fetchData = () => {
	fetch('http://localhost:4000/courses/all')
	.then(res => res.json())
	.then(data => {
		console.log(data)
		setAllCourses(data)
	})
}

useEffect(() => {
	fetchData()
}, [])

return(
	<Container>
		{
			(user.isAdmin === true) ?
			<AdminView coursesData = {allCourses} />
			:
			<UserView coursesData = {allCourses} />

		}
	</Container>
	)
}