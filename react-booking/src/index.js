import React from 'react';
import ReactDOM from 'react-dom';
import App from './App';

//Importt the Bootstrap CSS
import 'bootstrap/dist/css/bootstrap.min.css';

ReactDOM.render(
    <App />
    ,
  document.getElementById('root')
);

 //*const name = "Hello Peter!!";
// const user = {
//   firstName: "Peter",
//   lastName:  "Parker"
//               }

// const formatName = (user) => {
//   return user.firstName + " " + user.lastName;
// }

// const element = <h1>Hello, {formatName(user)}</h1>

// ReactDOM.render(element, document.getElementById('root'));
// */