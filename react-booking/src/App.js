import './App.css';
import {useState, useEffect} from 'react';
import AppNavbar from './Components/AppNavbar';

//pages
import Home from './pages/Home';
import Courses from './pages/Courses';
import Register from './pages/Register';
import LogIn from './pages/LogIn';
import Error from './pages/Error';
import LogOut from './pages/LogOut';

// //components
// import Counter from './Components/Counter';

//routing components
import { BrowserRouter as Router} from 'react-router-dom';
import  { Route, Switch } from 'react-router-dom';



//Bootstrap
import {Container} from 'react-bootstrap';

//React Content
import {UserProvider} from './UserContext';


/*
BrowserRouter component will enable us to simulate page navigation by synchronizing the shown
content and the shown URL in the web browser

Switchc component then decalres with Route we can go to

Route component will render components within the switch container based on the defined route

Exact property disables the partial matching for a route and makes sure that it only returns
the route if the path is an exact matcch to the current url

If exact and path is missing, the Route component will make it undefined route and will be
loaded into a specified focument.

*/
/*
React Context is a global state to the app. It is a way to make a particular


*/

function App() {

//add a state hook for user;
  const [user, setUser] = useState({
    accessToken: localStorage.getItem('accessToken'),
    email: localStorage.getItem('email'),
    isAdmin: localStorage.getItem('isAdmin') === 'true'

  })

//Function for clearing localStorage on logout
const unsetUser = () => {
  localStorage.clear()
}

//Used to check if the user information is properly stored upon logIn and the localstorage information is cleared upon logout
useEffect(()=> {
  console.log(user);
  console.log(localStorage)
}, [user])

//Provider Component that allows consuming components to subscribe to conext changes
  return (
      <UserProvider value={ {user, setUser, unsetUser}}>
    <Router>
      <AppNavbar/>
        <Container>
          <Switch>
            <Route exact path="/" component={Home} />
            <Route exact path="/courses"component={Courses}/>
            <Route exact path="/register"component={Register}/>
            <Route exact path="/login"   component={LogIn}/>
            <Route exact path="/logout" component={LogOut}/>
            <Route path="/" component= {Error} />
        </Switch>
      </Container>
    </Router>
    </UserProvider>
  );
}

export default App;

/*

With the React Fragment Component, we can g roup multiple components and avoid adding extra code



JSX Syntax
JSX or Javascript XML is an extension to t he syntax of JS. It allows us to write
HTML-like syntax within our React js projects and it cinldues JS features as well
Install the Js(Babel) linting for code readability
Ctrl + Shift + P
In the input field enter Package Control
Install Babel

*/